#!/usr/bin/env bash

# install docker
# https://docs.docker.com/engine/installation/linux/ubuntulinux/

# install docker-compose
# https://docs.docker.com/compose/install/

# install letsencrypt
# https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-16-04

# Generate SSL certificate for domain
# /opt/letsencrypt/letsencrypt-auto certonly --keep-until-expiring --standalone -d domain.example.com --email info@example.com

# Setup letsencrypt certificates renewing
# line="30 2 * * 1 /opt/letsencrypt/letsencrypt-auto renew >> /var/log/letsencrypt-renew.log"
# (crontab -u root -l; echo "$line" ) | crontab -u root -

# Rename SSL certificates
# https://community.letsencrypt.org/t/how-to-get-crt-and-key-files-from-i-just-have-pem-files/7348
cd /etc/letsencrypt/live/dos.grit.systems/
sudo cp privkey.pem domain.key
sudo cat cert.pem fullchain.pem > domain.crt
sudo chmod 777 domain.crt
sudo chmod 777 domain.key

#remove any container named registry
sudo docker stop registry &&

sudo docker rm registry 

# https://docs.docker.com/registry/deploying/
sudo docker run -d -p 7000:5000 --restart=always --name registry   -v /etc/letsencrypt/live/dos.grit.systems:/certs   -v /opt/docker-registry:/var/lib/registry   -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt   -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key -e REGISTRY_HTTP_ADDR=0.0.0.0:5000 -e REGISTRY_HTTP_HOST=https://dos.grit.systems  registry:2.1.1
  
# List images
# https://domain.example.com/v2/_catalog
