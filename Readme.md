Use the setup_registry.sh to setup for first time. 
If you didnt setup and run docker-compose an error will be thrown

To deploy an image run

Tag the image so that it points to your registry

docker image tag <image> <registry_url or 
localhost:7000>/your_image_tag 
e.g docker image tag ubuntu localhost:7000/myfirstimage


push it
docker push localhost:7000/myfirstimage

To view images  visit https://<registry_url>/v2/_catalog 
